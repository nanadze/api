<?php
ob_start();
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__)));

//this function loads Class files automatically 
function __autoload($path) { //לתוך הפונקציה נכנס שם המחלקה
	return include str_replace('_', '/', $path) . '.php';
}
$rest = new Rest(); //rest יצירת אוביקט מסוג 
$rest->process();
ob_end_flush();
